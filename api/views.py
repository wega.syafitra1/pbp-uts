from django.http.response import JsonResponse,HttpResponse
from django.shortcuts import render
from django.core import serializers
from django.contrib.auth import login, logout,authenticate
from django.views.decorators.csrf import csrf_exempt
from main.models import *
from main.forms import *
from content.models import *
from forum.models import *
from helpdesk.models import *
import json
# Create your views here.

@csrf_exempt
def all_courses_api(request):
    courses = Course.objects.all()
    response = serializers.serialize("json", courses )
    return HttpResponse(response, content_type='application/json')
    # return JsonResponse(response)

@csrf_exempt
def forum_post_api(request, id):
    course = Course.objects.get(id = id)
    data = serializers.serialize('json', ForumPost.objects.filter(course = course))
    return HttpResponse(data, content_type='application/json')

@csrf_exempt
def reply_api(request, id):
    post = ForumPost.objects.get(id = id)
    data = serializers.serialize('json', Reply.objects.filter(post = post))
    return HttpResponse(data, content_type='application/json')

@csrf_exempt    
def participants_json_api(request):
    data = serializers.serialize('json', User.objects.all())
    return HttpResponse(data, content_type='application/json')
    # return HttpResponse(Murid.objects.all())
@csrf_exempt
def participants_murid_json_api(request):
    data = serializers.serialize('json', Murid.objects.all())
    return HttpResponse(data, content_type='application/json')

@csrf_exempt
def participants_murid_user_api(request,id):
    murid = list(Murid.objects.filter(pk=id))
    user = list(User.objects.filter(id=id))
    gabung = murid+user
    data = serializers.serialize('json', gabung)
    return HttpResponse(data, content_type='application/json')

@csrf_exempt
def participants_pengajar_json_api(request):
    data = serializers.serialize('json', Pengajar.objects.all())
    return HttpResponse(data, content_type='application/json')

@csrf_exempt
def participants_pengajar_user_api(request,id):
    guru = list(Pengajar.objects.filter(pk=id))
    user = list(User.objects.filter(id=id))
    gabung = guru+user
    data = serializers.serialize('json', gabung)
    return HttpResponse(data, content_type='application/json')


@csrf_exempt
def contents_json_api(request,id):
    data = serializers.serialize('json', Course.objects.filter(id=id))
    return HttpResponse(data, content_type='application/json')
@csrf_exempt
def summary_json_api(request, id):
    data = serializers.serialize('json', SummaryPoints.objects.filter(course=id))
    return HttpResponse(data, content_type='application/json')
@csrf_exempt
def view_json_api(request, id):
    data = serializers.serialize('json', User.objects.filter(id=id))
    return HttpResponse(data, content_type='application/json')


@csrf_exempt
def login_request_api(request):
    if request.method == "POST":
        data = json.loads(request.body)
        print(data)
        username = data['username']
        password = data['password']
        user = authenticate(username=username, password=password)
        if user is not None :
            
            login(request,user)
            id = user.pk
            data = serializers.serialize('json', User.objects.filter(id=id))
            print(user)
            return HttpResponse(data, content_type='application/json')

        else:
            return JsonResponse({'res':'USER DOESNT EXIST'})
     
    else:
        return JsonResponse({'res':'must use post method'})
    

@csrf_exempt
def register_murid_api(request):
    if request.method == "POST":
        data = json.loads(request.body)
        print(data)
        username = data['username']
        password1 = data['password1']
        password2= data['password2']
        nama_depan= data['nama_depan']
        nama_belakang= data['nama_belakang']
        kota= data['kota']
        tanggal = data['tanggal_lahir']
        telepon = data['telepon']

        baru = User(username=username)
        baru.set_password(password1)
        baru.is_murid = True
        baru.first_name = nama_depan
        baru.last_name = nama_belakang
        baru.city = kota
        baru.save()

        murid = Murid.objects.create(user=baru)
        murid.phone_number = telepon
        murid.city = kota
        murid.dob = tanggal
        murid.save()

        res = serializers.serialize('json',  User.objects.filter(username=username))
        print(res)
        return HttpResponse(res, content_type='application/json')
     
    else:
        return JsonResponse({'res':'must use post method'})





@csrf_exempt
def register_guru_api(request):
    if request.method == "POST":
        data = json.loads(request.body)
        print(data)
        username = data['username']
        password1 = data['password1']
        password2= data['password2']
        nama_depan= data['nama_depan']
        nama_belakang= data['nama_belakang']
        orgs= data['instansi']

        baru = User(username=username)
        baru.set_password(password1)       
        baru.is_pengajar= True
        baru.first_name = nama_depan
        baru.last_name = nama_belakang
        baru.save()
        guru = Pengajar.objects.create(user=baru)
        guru.organization = orgs
        guru.save()

        res = serializers.serialize('json', User.objects.filter(username=username))
        print(res)
        return HttpResponse(res, content_type='application/json')
     
    else:
        return JsonResponse({'res':'must use post method'})


@csrf_exempt
def update_user_api(request,id):

    if request.method == "POST":
        data = json.loads(request.body)
        print(data)

        email = data['email']
        nama_depan= data['nama_depan']
        nama_belakang= data['nama_belakang']
        hobby= data['hobby']
        motto= data['motto']

        update = User.objects.get(id=id)   
        update.email = email
        update.first_name = nama_depan
        update.last_name = nama_belakang
        update.hobby = hobby
        update.motto = motto
        update.save()

        # update.first_name = fi


        res = serializers.serialize('json', User.objects.filter(id=id))
        print(res)
        return HttpResponse(res, content_type='application/json')
     
    else:
        return JsonResponse({'res':'must use post method'})

@csrf_exempt
def logout_view(request):
    logout(request)
    return JsonResponse({'res':'LOGOUT SUCCESSFUL'})



@csrf_exempt
def sections_api(request,id):
    course = Course.objects.get(id = id)
    data = serializers.serialize('json', Section.objects.filter(course=course))
    return HttpResponse(data, content_type='application/json')


@csrf_exempt
def add_section_api(request,id):
    # print("method "+request.method)
    # # print(request.GET)
    # print("body "+request.body)
    # print("post "+request.POST)

    if request.method == 'POST':
        data = json.loads(request.body)
        # print("data "+data)
        titleIn = data['title']
        baru = Section(title=titleIn)
        baru.save()

        course_obj = Course.objects.get(id=id)
        latest = Section.objects.last()
        latest.course = course_obj
        latest.save()

        
        data = serializers.serialize('json', Section.objects.filter(course=course_obj))
        return HttpResponse(data, content_type='application/json')
    else :
        return JsonResponse({'res':'must use post method'})

@csrf_exempt
def sections_posts_api(request,id):
    sect = Section.objects.get(id = id)
    data = serializers.serialize('json', sect.post_set.all())
    return HttpResponse(data, content_type='application/json')
    


@csrf_exempt
def add_section_post_api(request,id,sectionid):
    if request.method == 'POST':
        data = json.loads(request.body)
        titleIn = data['title']
        descriptionIn = data['description']
        baru = Post(title=titleIn,description=descriptionIn)
        baru.save()

        course_obj = Course.objects.get(id=id)
        section_obj = Section.objects.get(id=sectionid)
        latest = Post.objects.last()
        latest.course = course_obj
        latest.section = section_obj
        latest.save()
        
        data = serializers.serialize('json', section_obj.post_set.all())
        return HttpResponse(data, content_type='application/json')
    else :
        return JsonResponse({'res':'must use post method'})

@csrf_exempt
def content_api(request):
    content = Content.objects.all()
    data = serializers.serialize('json', content)
    return HttpResponse(data, content_type='application/json')

@csrf_exempt
def add_content_file_api(request,id):
    if request.method == 'POST':
        data = json.loads(request.body)
        title = data['title']
        fileurl = data['file']
    
        baru = Content.objects.create(title=title)
        baru.file = fileurl
        baru.save()

        course_obj = Course.objects.get(id=id)
        latest = Content.objects.last()
        latest.course = course_obj
        latest.save()
        
        data = serializers.serialize('json', Content.objects.all())
        return HttpResponse(data, content_type='application/json')
    else :
        return JsonResponse({'res':'must use post method'})

@csrf_exempt
def all_feedback_api(request):
    help = Helpdesk.objects.all()
    response = serializers.serialize("json", help )
    return HttpResponse(response, content_type='application/json')

@csrf_exempt
def add_feedback_api(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        sender = data['from']
        msg = data['msg']

        baru = Helpdesk.objects.create(froma=sender,message=msg)
        baru.save()

        data = serializers.serialize('json', Helpdesk.objects.all())
        return HttpResponse(data, content_type='application/json')
    else :
        return JsonResponse({'res':'must use post method'})


@csrf_exempt
def create_post_api(request,courseid, usrid):
    if request.method == 'POST':
        data = json.loads(request.body)
        title = data['title']
        content = data['content']
        baru = ForumPost.objects.create(title=title, content=content)
        baru.save()

        author = User.objects.get(pk=usrid)
        course = Course.objects.get(id=courseid)
        latest = ForumPost.objects.last()
        latest.author = author
        latest.course = course
        latest.save()
        data = serializers.serialize('json', ForumPost.objects.filter(course=course))
        return HttpResponse(data, content_type='application/json')
    
    else :
        return JsonResponse({'res':'must use post method'})

@csrf_exempt
def reply_api(request,id,usrid):
    if request.method=="POST":
        data = json.loads(request.body)
        title = data['title']
        content = data['content']

        baru=Reply.objects.create(title=title,content=content)
        baru.save()
        
        author = User.objects.get(pk=usrid)
        post_obj = ForumPost.objects.get(id = id)
        latest = Reply.objects.last()
        latest.author = author
        latest.post = post_obj
        latest.save()
        data = serializers.serialize('json', Reply.objects.filter(post=post_obj))
        return HttpResponse(data, content_type='application/json')
    else :
        return JsonResponse({'res':'must use post method'})
