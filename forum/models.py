from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.timezone import *
from main.models import *
# Create your models here.

class ForumPost(models.Model):
    course = models.ForeignKey(Course,null=True, on_delete = models.CASCADE)
    author = models.ForeignKey(User,null=True, on_delete = models.CASCADE, default = 1)
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=150)
    content = models.CharField(max_length=5000)
    timestamp = models.DateTimeField(default=now)

    def get_courseid(self):
        return self.course.id

class Reply(models.Model):
    author = models.ForeignKey(User,null=True, on_delete = models.CASCADE)
    id = models.BigAutoField(primary_key=True)
    post = models.ForeignKey(ForumPost,null=True, on_delete=models.CASCADE)
    content = models.CharField(max_length=5000)
    timestamp = models.DateTimeField(default=now)

    def get_postid(self):
        return self.post.id

