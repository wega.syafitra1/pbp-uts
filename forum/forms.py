from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm
from django.db.models import fields
from django.forms import widgets
from .models import *
from django.db import transaction
from django.utils.translation import gettext_lazy as __
import datetime

bootstrap_form = {"class" : "form-control"}

class PostCreation(forms.ModelForm):
    class Meta:
        model = ForumPost
        fields = ('title','content')

class ReplyForm(forms.ModelForm):
    class Meta:
        model = Reply
        fields = ('content',)