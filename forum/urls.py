from django.urls import include, path

from . import views

app_name = 'forum'

urlpatterns = [
    path('^course/<id>/', include('main.urls')),
    path('main/<courseid>/', views.forum_main, name='forum_main'),
    path('main/<courseid>/replyjson/', views.reply_json, name='forum_mainjson'),
    path('main/<courseid>/add-post/', views.create_post, name='create_post'),
    path('', views.forum_main, name='forum_main'),
    path('<id>/', views.discussion, name='discussion'),
    # path('add-post/', views.create_post, name='create_post'),
    path('<id>/reply/', views.reply, name='reply'),

]
