from django.shortcuts import render,redirect
from django.contrib.auth import login, logout,authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.views.generic import CreateView
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.core.paginator import Paginator
from django.http import HttpResponse
from django.core import serializers

from .forms import *
from .models import *

from main.models import *
# Create your views here.

@login_required(login_url='/login/')
def forum_main(request,courseid):
    course = Course.objects.get(id = courseid)
    posts = ForumPost.objects.filter(course=course)
    page = request.GET.get('page', 1)

    paginator = Paginator(posts, 5)
    try:
        nposts = paginator.page(page)
    except PageNotAnInteger:
        nposts = paginator.page(1)
    except EmptyPage:
        nposts = paginator.page(paginator.num_pages)
    
    context = {'nposts':nposts, 'course':course}
    return render(request,'forum.html', context)

@login_required(login_url='/login/')
def discussion(request, id):
    discussion = ForumPost.objects.get(id = id)
    replies = Reply.objects.filter(post = discussion)
    context = {'discussion':discussion, 'replies':replies}
    return render(request,'discussion.html', context)

@login_required(login_url='/login/')
def create_post(request,courseid):
    form = PostCreation(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            author = request.user
            # author_obj = Foru.objects.get(user=author)
            course = Course.objects.get(id=courseid)
            latest = ForumPost.objects.last()
            latest.author = author
            latest.course = course
            latest.save()
            return redirect('/forum/main/%s'%courseid)
        else:
            return JsonResponse({'status':'Error', 'msg': 'Post gagal dibuat'})

    context = {'form':form}
    return render(request, 'createpost.html', context)


@login_required(login_url='/login/')
def reply(request,id):
    form = ReplyForm(request.POST or None)
    if request.method=="POST":
        if form.is_valid():
            form.save()
            author = request.user
            # author_obj = User.objects.get(user=author)
            post_obj = ForumPost.objects.get(id = id)
            latest = Reply.objects.last()
            latest.author = author
            latest.post = post_obj
            latest.save()
            return redirect('/forum/%s'%id)
        else:
            return JsonResponse({'status':'Error', 'msg': 'Post gagal dibuat'})

    context = {'form':form}
    return render(request, 'reply.html', context)

def reply_json(request,courseid):
    data = serializers.serialize('json', Reply.objects.filter(post=ForumPost.objects.get(course=courseid)))
    return HttpResponse(data, content_type='application/json')
