from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import *

app_name = 'ujian'

route = DefaultRouter()
route.register('ujian', QuizView, basename="ujian")
route.register('question', ViewQuestion, basename="question")
route.register('answer', ViewAnswer, basename="answer")

urlpatterns = [
    path('', QuizList.as_view(), name='main-view'),
    path('lecturer-home-quiz/', QuizListForLecturer.as_view(), name='main-view-lecturer'),
    path('lecturer-home-quiz/<pk>/', detail, name='quiz-view-lecturer'),
    path('add-quiz/', add_quiz, name='add-quiz'),
    path('<pk>/', quiz_view, name='quiz-view'),
    path('<pk>/data/', quiz_data, name='quiz-data'),
    path('<pk>/save/', save_quiz, name='save-quiz'),
    path('detail/<pk>/', detail, name='detail-quiz'),
    path('<pk>/view-questions/', QuestionView.as_view(), name='view-questions'),
    path('<pk>/view-questions/data/', quiz_view_questions_data, name='quiz-view-questions-data'),
    path('delete-quiz/<pk>/', delete_quiz, name='delete-quiz'),
    path('update/<pk>/', QuizUpdate.as_view(), name='update-quiz'),
    path('', showlist, name='show-list'),
    path('create_question/<pk>/', createQuestion, name="create_question"),
    path('update-question/<pk>/', QuestionUpdate.as_view(), name='update-question'),
    path('view-answer/<pk>/', index, name='view-answer'),
    path('delete_question/<pk>/', deleteQuestion, name="delete_question"),
    path('quizapi/', include(route.urls))
]