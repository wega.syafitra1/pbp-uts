console.log("hlo")
const url = window.location.href
const qBox = document.getElementById("quiz-box")
const gradeBox = document.getElementById("grade-box")
const resultBox = document.getElementById("result-box")
const timerBox = document.getElementById("timer")
const backBox = document.getElementById("btn")
const submitBox = document.getElementById("submit-box")
let tmr

const timer = (time) => {
    let min = time-1
    let secs = 60
    let secondsToDisplay
    let minutesToDisplay
    if (time.toString().length<2){
        timerBox.innerHTML = `<b>0${time}:00</b>`
    }
    else{
        timerBox.innerHTML = `<b>${time}:00</b>`
    }
    tmr = setInterval(()=>{
        secs--
        if (secs < 0){
            secs = 59
            min--
        }
        if (min.toString().length<2){
            minutesToDisplay = '0'+min
        }
        else{
            minutesToDisplay = min
        }
        if (secs.toString().length<2){
            secondsToDisplay = '0'+secs
        }
        else{
            secondsToDisplay = secs
        }
        if (min === 0 && secs === 0){
            timerBox.innerHTML = "<b>00:00</b>"
            setTimeout(()=>{
                clearInterval(tmr)
                sendData()
            }, 500)
        }
        timerBox.innerHTML = `<b>${minutesToDisplay}:${secondsToDisplay}</b>`
    }, 1000)
}

var count = 1

$.ajax({
    type: 'GET',
    url: `${url}data`,
    success: function(response){
        const data = response.data
        backBox.style.display="none"
        data.forEach(el => {
            for (const [question, answers] of Object.entries(el)){
                qBox.innerHTML += `
                    <hr>
                    <div class="mb-2">
                        <b>${count}. ${question}</b>
                    </div>
                `
                answers.forEach(a=>{
                    qBox.innerHTML += `
                        <div>
                            <input type="radio" class="ans" id="${question}-${a}" name="${question}" value="${a}">
                            <label for="${question}">${a}</label>
                        </div>
                    `
                })
                count = count + 1
            }
        });
        timer(response.time)
    },
    error: function(error){
        console.log(error)
    }
})
const quizIdentity = document.getElementById('quiz-form')
const csrf = document.getElementsByName('csrfmiddlewaretoken')

const sendData = () => {
    const answers = [...document.getElementsByClassName('ans')]
    const data = {}
    data['csrfmiddlewaretoken'] = csrf[0].value
    answers.forEach(an=>{
        if (an.checked){
            data[an.name] = an.value
        } else{
            if (!data[an.name]){
                data[an.name] = null
            }
        }
    })

    $.ajax({
        type: 'POST',
        url: `${url}save/`,
        data: data,
        success: function(response){
            console.log("clicked submit")
            clearInterval(tmr)
            const results = response.results
            quizIdentity.classList.add('not-visible')
            backBox.style.display="inline"
            gradeBox.innerHTML = `<hr>`
            gradeBox.innerHTML += `<b>Your final grade for this quiz is ${response.score.toFixed(2)}</b> `
            
            results.forEach(result=>{
                const reviewDiv = document.createElement("div")
                const resultDiv = document.createElement("div")
                for (const [question, rsp] of Object.entries(result)){
                    const correct_answer = rsp['correct_answer']
                    reviewDiv.innerHTML += question

                    const cls = ['container', 'p-7', 'text-light', 'h6']
                    resultDiv.classList.add(...cls)

                    if (rsp['answered']=='not answered'){
                        resultDiv.innerHTML+= `Your answer is incorrect. <br>
                        The correct answer is: ${correct_answer}`
                        resultDiv.classList.add('bg-danger')
                    }
                    else{
                        const answer = rsp['answered']
                        if (answer==correct_answer){
                            resultDiv.classList.add('bg-success')
                            resultDiv.innerHTML+= `Your answer is correct. <br>
                            The correct answer is: ${correct_answer}`
                        }
                        else{
                            resultDiv.classList.add('bg-danger')
                            resultDiv.innerHTML+= `Your answer (${answer}) is incorrect. <br>
                            The correct answer is: ${correct_answer}`
                        }
                    }
                }
                resultBox.append(reviewDiv)
                resultBox.append(resultDiv)

            })
        },
        error: function(error){
            console.log(error)
        }
    })

}

quizIdentity.addEventListener('submit',e=>{
    e.preventDefault()
    sendData()
})