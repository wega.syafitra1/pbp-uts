// const viewBtns = [...document.getElementsByClassName('view-button')]
// const deleteBtns = [...document.getElementsByClassName('btn-danger')]
const modalBtns = [...document.getElementsByClassName('modal-button')]
const deleteBtns = document.getElementById('delete-button')
const modalBody = document.getElementById('modal-body-confirm')

modalBtns.forEach(modalBtn=> modalBtn.addEventListener('click', ()=>{
    const pk = modalBtn.getAttribute('data-pk')
    console.log(pk)
    const name = modalBtn.getAttribute('data-quiz')
    const topic = modalBtn.getAttribute('data-topic')
    const numQuestions = modalBtn.getAttribute('data-questions')
    const scoreToPass = modalBtn.getAttribute('data-pass')
    const time = modalBtn.getAttribute('data-time')
    modalBody.innerHTML = `
        <div class="h5 mb-3">Are you sure want to delete <b>${name}-${topic}</b>?</div>
        <div class="text">
            <ul>
                <li>Total Questions: <b>${numQuestions}</b></li>
                <li>Score to pass: <b>${scoreToPass}</b></li>
                <li>Time Limit: <b>${time} minutes</b></li>
            </ul>
        </div>
    `
    console.log('kkk')
    deleteBtns.addEventListener('click', ()=>{
        var id = $(this).val()
        var csrf=$('input[name=csrfmiddlewaretoken').val()
        $.ajax({
            url:'.',
            method: "POST",
            data:{
                id,
                csrfmiddlewaretoken:csrf,
            },
        })
    })
}))