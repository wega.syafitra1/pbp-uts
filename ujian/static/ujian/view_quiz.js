const url = window.location.href
const qBox = document.getElementById("quiz-box")
const deleteBtn = document.getElementById("delete_btn")
let gradeBox = document.getElementById("grade-box")
console.log('test')

var count = 1

$.ajax({
    type: 'GET',
    url: `${url}data`,
    success: function(response){
        const data = response.data
        const totalQuestions = data.length
        gradeBox.innerHTML += `
        Number of questions: ${totalQuestions}
        `
        gradeBox.innerHTML += `
            <div class="mt-3">Click checkbox to delete question</div>

        `
        data.forEach(el => {
            console.log(el)
            for (const [question, answers] of Object.entries(el)){
                const pk = answers[0]
                qBox.innerHTML += `
                    <hr>
                    <div class="mb-2">
                        <input type="checkbox" class="ans" id="delete_question" name="${question}" value="${pk}">
                        <a href="http://localhost:8000/quiz/update-question/${pk}/" style="text-decoration:none;">${question}</a>
                    </div>
                    <div class="mb-1">
                        <a href="http://localhost:8000/quiz/view-answer/${pk}/" class="btn btn-secondary">View Answer</a>
                    </div>
                `
            }
        });
    },
    error: function(error){
        console.log(error)
    }
})

$(document).ready(function(){
    $('#delete_btn').click(function(){
        if(confirm("Are you sure want to delete the question?")){
            var pk=[]
            var csrf=$('input[name=csrfmiddlewaretoken').val()
            $(':checkbox:checked').each(function(i){
                pk[i]=$(this).val()
            })
            if(pk.length===0){
                alert("No question selected")
            }
            else{
                $.ajax({
                    method: "POST",
                    data:{
                        pk,
                        csrfmiddlewaretoken:csrf
                    },
                })
                window.location.href = "http://localhost:8000/quiz/lecturer-home-quiz/"
            }
            console.log("masukk sini yey")
        }

    })
})