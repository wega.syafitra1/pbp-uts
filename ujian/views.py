from django.db import models
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import redirect, render, get_object_or_404
from django.views.generic.base import View
from .forms import QuizForm, QuestionForm
from django.views.generic import ListView
from rest_framework.decorators import api_view
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from .serializers import AnswerSerializers, QuestionSerializers, QuizSerializers
from .models import Quiz, Question, Answer, Result
from django.forms import inlineformset_factory

class QuizList(ListView):
    model = Quiz
    template_name = 'ujian/main.html'

class QuizListForLecturer(ListView):
    model = Quiz
    template_name = 'ujian/lecturer_home_quiz.html'

def quiz_view(request, pk):
    quiz = Quiz.objects.get(pk=pk)
    return render(request, 'ujian/quiz.html', {'obj': quiz})


def quiz_data(request, pk):
    quiz = Quiz.objects.get(pk=pk)
    questions = []
    for question in quiz.get_questions():
        answers = []
        for ans in question.get_answers():
            answers.append(ans.text)
        questions.append({str(question): answers})
        print(question)
        print(answers)
    return JsonResponse({'data': questions, 'time': quiz.time})

def save_quiz(request,pk):
    if request.is_ajax():
        data = request.POST
        questions = []
        data1 = dict(data.lists())
        data1.pop('csrfmiddlewaretoken')
        print(data1)

        for key in data1.keys():
            q = Question.objects.get(text=key)
            questions.append(q)

        poin = 0
        user = request.user
        quiz = Quiz.objects.get(pk=pk)
        mul = 100/quiz.number_of_questions
        result = []
        correct = None

        for q in questions:
            selected = request.POST.get(q.text)
            q_answers = Answer.objects.filter(question=q)
            if selected != "":
                for a in q_answers:
                    if selected == a.text:
                        if a.correct:
                            poin += 1
                            correct = a.text
                    else:
                        if a.correct:
                            correct = a.text
                result.append({str(q): {'correct_answer': correct, 'answered': selected}})
            else:
                for a in q_answers:
                    if a.correct:
                            correct = a.text
                result.append({str(q): {'correct_answer': correct, 'answered': 'not answered'}})
                

        grade = poin * mul
        Result.objects.create(quiz=quiz, user=user, score=grade)

        if grade >= quiz.required_score_to_pass:
            return JsonResponse({'passed': True, 'score': grade, 'results': result})
        else:
            return JsonResponse({'passed': False, 'score': grade, 'results': result})

def add_quiz(request):
    context ={}
      
    if request.method == 'POST':
        form = QuizForm(request.POST) 
        print(request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/quiz/lecturer-home-quiz/')
    else:
        form = QuizForm()

    context['form']= form
    return render(request, "ujian/form.html", context)

class QuizUpdate(View):
    def get(self, request, pk):
        quiz = Quiz.objects.get(pk=pk)
        form = QuizForm(instance=quiz)
        return render(request, 'ujian/quiz_update.html', context={'form': form, 'quiz':quiz})

    def post(self, request, pk):
        quiz = get_object_or_404(Quiz, pk=pk)
        form = QuizForm(request.POST, instance=quiz)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/quiz/lecturer-home-quiz/')

        return render(request, 'ujian/quiz_update.html', context={'form': form, 'quiz':quiz})

def delete_quiz(request, pk):
    quiz = Quiz.objects.get(pk=pk)
    context = {'quiz': quiz}
    if request.method == "POST":
        quiz.delete()
        return HttpResponseRedirect('/quiz/lecturer-home-quiz/')
    return render(request, "ujian/delete.html", context)

def detail(request,pk):
    quiz=get_object_or_404(Quiz,pk=pk)
    if request.method == "POST":
        quiz.delete()
        return HttpResponseRedirect('/quiz/lecturer-home-quiz/')
    return render(request,"ujian/lecturer_home_quiz.html",{"obj":quiz})

class QuestionView(View):
    def get(self, request, pk):
        quiz = Quiz.objects.get(pk=pk)
        return render(request, 'ujian/view_question.html', {'obj': quiz})

    def post(self, request, *args, **kwargs):
        if request.method == "POST":
            question = request.POST.getlist('pk[]')
            for pk in question:
                question = Question.objects.get(pk=pk)
                question.delete()
            return HttpResponseRedirect('/quiz/lecturer-home-quiz/')

def quiz_view_questions_data(request, pk):
    quiz = Quiz.objects.get(pk=pk)
    questions = []
    for question in quiz.get_questions():
        print(question.text)
        print(question.pk)
        answers = []
        answers.append(question.pk)
        for ans in question.get_answers():
            answers.append(ans.text)
        questions.append({str(question): answers})
    print(questions)
    return JsonResponse({'data': questions, 'time': quiz.time})


def index(request, pk):
    question = Question.objects.get(pk=pk)
    AnswerFormset = inlineformset_factory(Question, Answer, fields=('text','correct',), extra=3)
    formset = AnswerFormset(instance=question)
    url = '/quiz/view-answer/' + pk + '/'
    # url = '/quiz/' + pk + '/view-questions/'

    if request.method == 'POST':
        formset = AnswerFormset(request.POST, instance=question)
        if formset.is_valid():
            formset.save()
            return HttpResponseRedirect(url)

    return render(request, 'ujian/index.html', {'formset' : formset, 'pk':pk, 'question':question})

def showlist(request):
    results = Quiz.objects.all()
    return render(request, "ujian/question.html", {"showquiz":results})

def createQuestion(request, pk):
    quiz = Quiz.objects.get(pk=pk)
    form = QuestionForm(initial={'quiz': quiz})
    if request.method =='POST':
        form = QuestionForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/quiz/lecturer-home-quiz/')

    context = {'form': form}
    return render(request, "ujian/question.html", context)

class QuestionUpdate(View):
    def get(self, request, pk):
        question = Question.objects.get(pk=pk)
        form = QuestionForm(instance=question)
        return render(request, 'ujian/question_update.html', context={'form': form, 'question':question})

    def post(self, request, pk):
        question = get_object_or_404(Question, pk=pk)
        form = QuestionForm(request.POST, instance=question)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/quiz/lecturer-home-quiz/')

        return render(request, 'ujian/question_update.html', context={'form': form, 'question':question})

def updateQuestion(request, pk):
    question = Question.objects.get(id=pk)
    form = QuestionForm(instance=question)
    if request.method =='POST':
        form = QuestionForm(request.POST, instance=question)
        if form.is_valid():
            form.save()
            return redirect('/quiz/lecturer-home-quiz/')

    context = {'form': form}
    return render(request, "ujian/question.html", context)

def deleteQuestion(request, pk):
    question = Question.objects.get(id=pk)
    if request.method == 'POST':
        question.delete()
        return HttpResponseRedirect('/quiz/lecturer-home-quiz/')
        
    context = {'question': question}
    return render(request, "quiz/delete.html", context)

class QuizView(ModelViewSet):
    queryset = Quiz.objects.all().order_by('-id')
    serializer_class = QuizSerializers

class ViewQuestion(ModelViewSet):
    serializer_class = QuestionSerializers

    def get_queryset(self):
        queryset = Question.objects.all().order_by('-id')
        return queryset

    def retrieve(self, request, *args, **kwargs):
        params = kwargs
        questions = Question.objects.filter(quiz_id=params['pk'])
        serializer = QuestionSerializers(questions, many=True)
        return Response(serializer.data)

class ViewAnswer(ModelViewSet):
    serializer_class = AnswerSerializers

    def get_queryset(self):
        queryset = Answer.objects.all().order_by('-id')
        return queryset

    def retrieve(self, request, *args, **kwargs):
        params = kwargs
        answers = Answer.objects.filter(question_id=params['pk'])
        serializer = AnswerSerializers(answers, many=True)
        return Response(serializer.data)