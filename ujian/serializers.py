from rest_framework import serializers
from .models import Answer, Question, Quiz

class QuizSerializers(serializers.ModelSerializer):
    class Meta:
        model = Quiz
        fields = "__all__"

class QuestionSerializers(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = "__all__"

class AnswerSerializers(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = "__all__"