from django import forms
from .models import Quiz, Question
from django.forms import ModelForm

class QuizForm(forms.ModelForm):
    class Meta:
        model = Quiz
        fields = "__all__"

class QuestionForm(ModelForm):
    class Meta:
        model = Question
        fields = '__all__'
