from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Helpdesk)
admin.site.register(HelpdeskAnon)
admin.site.register(FAQ)