from django import forms
from .models import Helpdesk, HelpdeskAnon
from main.models import *

class HelpDesk(forms.ModelForm):
    class Meta:
        model = Helpdesk
        fields = '__all__'

class HelpDeskAnon(forms.ModelForm):
    class Meta:
        model = HelpdeskAnon
        fields = '__all__'