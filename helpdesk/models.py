from django.db import models

# Create your models here.
class Helpdesk(models.Model):
    froma = models.TextField()
    message = models.TextField()

class HelpdeskAnon(models.Model):
    message = models.TextField()

class FAQ(models.Model):
    question = models.TextField()
    answer = models.TextField()