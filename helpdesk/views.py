from django.shortcuts import render
from .models import *
from .forms import *
from django.http import HttpResponse
from django.http.response import JsonResponse

# Create your views here.
def helpdesk(request):
    if request.user.is_authenticated:
        return render(request, 'helpdesk.html')
    else:
        return HttpResponse('You are not authorized to view this page')

def faq(request):
    if request.user.is_authenticated:
        faq = FAQ.objects.all().values()
        response = {'faq' : faq} 
        return render(request, 'faq.html', response)
    else:
        return HttpResponse('You are not authorized to view this page')


def add(request):
    if request.user.is_authenticated:
        form = HelpDesk()
        if request.is_ajax():
            form = HelpDesk(request.POST)
            if form.is_valid():
                form.save()
                return JsonResponse({
                    'message' : 'success'
                })
        return render(request, 'helpdesk_add.html', {'form' : form})
    else:
        return HttpResponse('You are not authorized to view this page')


def addAnon(request):
    if request.user.is_authenticated:
        form = HelpDeskAnon()
        if request.is_ajax():
            form = HelpDeskAnon(request.POST)
            if form.is_valid():
                form.save()
                return JsonResponse({
                    'message' : 'success'
                })
        return render(request, 'helpdesk_add_anon.html', {'form' : form})
    else:
        return HttpResponse('You are not authorized to view this page')


adminEmail = 'admin@learningapp.com'
adminNumber = '081234567890'
adminAddress = 'Jalan Kemerdekaan No.45'

def adminContact(request):
    if request.user.is_authenticated:
        response = {'email': adminEmail,
                    'number': adminNumber,
                    'address': adminAddress}
        return render(request, 'contact_admin.html', response)
    
    else:
        return HttpResponse('You are not authorized to view this page')


name = 'Anonymous'
def feedbackList(request):
    if request.user.is_authenticated:
        list = Helpdesk.objects.all().values()
        listAnon = HelpdeskAnon.objects.all().values()
        response = {'list' : list,'listAnon' : listAnon, 'name' : name} 
        return render(request, 'feedback_list.html', response)
    
    else:
        return HttpResponse('You are not authorized to view this page')
