from django.urls import path
from .views import *

urlpatterns = [
    path('', helpdesk, name='helpdesk'),
    path('faq/', faq, name='faq'),
    path('add/', add, name='add'),
    path('add-anon/', addAnon, name='addAnon'),
    path('list/', feedbackList, name='feedbackList'),
    # path('done/', done, name='done'),
    path('contact-admin/', adminContact, name='contact_admin')
]