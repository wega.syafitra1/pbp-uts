//For doing AJAX post
$("#submit").click(function(e) {
    e.preventDefault();
    var icon = $('#inputFile').val();


//This is the Ajax post.Observe carefully. It is nothing but details of where_to_post,what_to_post
$.ajax({
    url : "/add", // the endpoint,commonly same url
    type : "POST", // http method
    data : { csrfmiddlewaretoken : csrftoken,
    icon: icon
    }, // data sent with the post request
        // handle a successful response
        success : function(json) {
        console.log(json); // another sanity check
        },
        // handle a non-successful response
        error : function(xhr,errmsg,err) {
        console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
});