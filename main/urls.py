from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('register/', views.register, name='register'),
    path('register/murid', views.murid_register.as_view(), name='register_murid'),
    path('register/pengajar', views.pengajar_register.as_view(), name='register_pengajar'),
    path('login/', views.login_request, name='login'),
    path('logout/',views.logout_view, name='logout'),
    path('add-course/', views.add_course, name='add_course'),
    path('all-courses/', views.all_courses, name='all_courses'),
    path('course/', views.all_courses, name='course'),
    path('course/<id>/', views.course_detail, name='course<id>'),
    path('course/<id>/contentsjson', views.contents_json, name='course<id>json'),
    path('course/<id>/participantsjson/', views.participants_json, name='course<id>participants'),
    path('course/<id>/add-section/', views.course_add_section, name='course<id>addsection'),
    path('course/<id>/add-summary/', views.course_add_summary, name='course<id>addsummary'),
    path('course/<id>/add-post/<sectionid>/', views.course_add_post, name='course<id>addpost<sectionid>'),
    path('course/<id>/unenroll/', views.course_unenroll, name='course<id>unenroll'),
    path('course-summary/<id>/', views.course_summary, name='course<id>summary'),
    path('course-summary/<id>/summaryjson', views.summary_json, name='course<id>summary'),
    path('course-summary/<id>/enroll/', views.course_enroll, name='course<id>enroll'),
    path('coursesbyme/', views.coursesbyme, name='coursesbyme'),
    path('coursestaken/', views.coursestaken, name='coursestaken'),
    path('profile/', views.detail_view, name='profile'),
    path('profileupdate/<id>/', views.update_view, name='profileupdate<id>'),
    path('profile/<id>/viewjson', views.view_json, name='profilejson'),
]
