from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models.base import ModelState
from django.db.models.deletion import CASCADE
from django.forms.widgets import Widget

# Create your models here.
class User(AbstractUser):
    # id = models.BigAutoField(primary_key=True)
    is_admin = models.BooleanField("is admin",default=False)
    is_pengajar = models.BooleanField("is pengajar",default=False)
    is_murid = models.BooleanField("is murid" ,default=False)
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)
    city = models.CharField(max_length=128, null=True)

    # Gak harus
    zip_code = models.IntegerField(default=0, null=True)
    hobby = models.CharField(max_length=128,default="", blank=True)
    motto = models.CharField(max_length=128, default="", blank=True)

    # last_login = models.DateTimeField(null=True)

    @property
    def get_name(self):
        return self.first_name+" "+self.last_name
    # @property
    # def get_id(self):
    #     return self.id

class Pengajar(models.Model):
    # id = models.BigAutoField(primary_key=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    organization = models.CharField(max_length=128)

    # Gak harus
    specialty = models.CharField(max_length=250,  default="" , blank=True)
    


class Course(models.Model):
    # id = models.BigAutoField(primary_key=True)
    author = models.ForeignKey(Pengajar, null=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=64)
    subtitle = models.CharField(max_length=64, blank=True)
    description = models.TextField(max_length=3000)


    
class SummaryPoints(models.Model):
    course = models.ForeignKey(Course, null=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=128)


class Section(models.Model):
    course = models.ForeignKey(Course,null=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=128,default="section name")

class Post(models.Model):
    id = models.BigAutoField(primary_key=True)
    course = models.ForeignKey(Course, null=True, on_delete=models.CASCADE)
    section = models.ForeignKey(Section, null=True, on_delete=models.CASCADE)

    title= models.CharField(max_length=128)
    description = models.TextField(max_length=10000)
    # image = models.ImageField(upload_to="posts")

class Murid(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    phone_number = models.CharField(max_length=20)
    city = models.CharField(max_length=32)
    dob = models.DateField(null=True)

    # relation
    course_taken = models.ManyToManyField(Course)