from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm
from django.db.models import fields
from django.forms import widgets
from .models import *
from django.db import transaction
from django.utils.translation import gettext_lazy as __
import datetime

bootstrap_form = {"class" : "form-control"}

class PengajarCreation(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User

    username = forms.CharField(required=True, widget=forms.TextInput(attrs=bootstrap_form))
    password1 = forms.PasswordInput()
    password2 = forms.PasswordInput()

    first_name = forms.CharField(label="Nama Depan", required=True, widget=forms.TextInput(attrs=bootstrap_form))
    last_name = forms.CharField( label="Nama Belakang",required=True, widget=forms.TextInput(attrs=bootstrap_form))
    organization = forms.CharField(label="Nama Sekolah atau Universitas",required=True, widget=forms.TextInput(attrs=bootstrap_form))

    @transaction.atomic
    def save(self):
        #add user
        user = super().save(commit=False)
        user.first_name = self.cleaned_data.get('first_name')
        user.last_name = self.cleaned_data.get('last_name')
        user.is_pengajar = True
        user.save()
        #add user as Pengajar
        pengajar = Pengajar.objects.create(user=user)
        pengajar.organization = self.cleaned_data.get('organization')
        pengajar.save()
        return pengajar


class MuridCreation(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        # fields = ('username','password','c')
        # widgets = {
        #     'username' : forms.TextInput(attrs=bootstrap_form),
        #     'password' : forms.PasswordInput(attrs=bootstrap_form),
        #     'password2' : forms.TextInput(attrs=bootstrap_form),
        # }

    # Mungkin perlu ganti yng bagian user
    username = forms.CharField(required=True, widget=forms.TextInput(attrs=bootstrap_form))
    password1 = forms.PasswordInput()
    password2 = forms.PasswordInput()

    first_name = forms.CharField(label="Nama Depan",required=True, widget=forms.TextInput(attrs=bootstrap_form))
    last_name = forms.CharField(label="Nama Belakang",required=True, widget=forms.TextInput(attrs=bootstrap_form))
    phone_number = forms.CharField(label="Nomor Telepon", required=True, widget=forms.TextInput(attrs=bootstrap_form))
    city = forms.CharField(label="Kota Asal", required=True, widget=forms.TextInput(attrs=bootstrap_form))
    dob = forms.DateField(label="Tanggal Lahir",required=True)

    
    @transaction.atomic
    def save(self):
        #add user
        user = super().save(commit=False)
        user.first_name = self.cleaned_data.get('first_name')
        user.last_name = self.cleaned_data.get('last_name')
        user.is_murid = True
        user.save()
        #add user as Murid
        murid = Murid.objects.create(user=user)
        murid.phone_number = self.cleaned_data.get('phone_number')
        murid.city = self.cleaned_data.get('city')
        murid.dob = self.cleaned_data.get('dob')
        murid.save()
        return murid

class CourseCreation(forms.ModelForm):
    class Meta:
        model = Course
        fields = ['title', 'subtitle','description']

class ProfileForm(forms.ModelForm):
 
    # create meta class
    class Meta:
        # specify model to be used
        model = User
 
        # specify fields to be used
        fields = ['first_name', 'last_name', 'hobby', 'city', 'motto']  

class SectionForm(forms.ModelForm):
    class Meta :
        model = Section
        fields = ['title']

class PostCreateForm(forms.ModelForm):
    class Meta :
        model = Post
        fields = ['title','description']

class SummaryPointsForm(forms.ModelForm):
    class Meta :
        model = SummaryPoints
        fields = ['title']
         
