from django.contrib import admin
from .models import *
from django.apps import apps

app = apps.get_app_config('main')
appforum = apps.get_app_config('forum')

for model_name, model in app.models.items():
    admin.site.register(model)
    
for model_name, model in appforum.models.items():
    admin.site.register(model)


# admin.site.register(User)
# admin.site.register(Pengajar)
# admin.site.register(Murid)
# admin.site.register(Course)
# admin.site.register(Post)
