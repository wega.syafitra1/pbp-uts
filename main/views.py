from django.http import JsonResponse
from django.shortcuts import render,redirect, get_object_or_404
from django.http.response import HttpResponse
from django.contrib.auth import login, logout,authenticate
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.contrib.auth.forms import AuthenticationForm
from django.views.generic import CreateView
from django.contrib import messages
from main.forms import *
from django.http import HttpResponse
from django.core import serializers
from main.models import *
from forum.models import *


def home(request):
    # if request.user.is_authenticated:
    #     return render(request, 'main/logged.html')
    # else:
    return render(request, 'main/home.html')

def register(request):
    return render(request, 'main/register.html')

class murid_register(CreateView):
    model = User
    form_class = MuridCreation
    template_name = 'main/register_murid.html'

    def form_valid(self, form):
        user = form.save()
        # login(self.request, user)
        return redirect('/')


class pengajar_register(CreateView):
    model = User
    form_class = PengajarCreation
    template_name = 'main/register_pengajar.html'

    def form_valid(self, form):
        user = form.save()
        # login(self.request, user)
        return redirect('/')

def login_request(request):
    if request.method=='POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None :
                if 'next' in request.POST:
                    login(request,user)
                    return redirect(request.POST.get('next'))

                login(request,user)
                return redirect('/')
            else:
                messages.error(request,"Invalid username or password")
        else:
                messages.error(request,"Invalid username or password")

    return render(request, 'main/login.html',context={'form':AuthenticationForm()})

@login_required(login_url="/login")
def logout_view(request):
    logout(request)
    return redirect('/')
@login_required(login_url="/login")
def add_course(request):
    form = CourseCreation(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            user_author = request.user
            author_obj = Pengajar.objects.get(user=user_author)
            form.save()
            # ubah course authornya
            latest = Course.objects.last()
            latest.author = author_obj
            latest.save()
            return redirect('/')
    context = {'form':form}
    return render(request, 'main/addcourse.html', context)
@login_required(login_url="/login")
def all_courses(request):
    courses = Course.objects.all()
    context = {'courses':courses}
    return render(request,'main/allcourses.html', context)

@login_required(login_url="/login")
def coursesbyme(request):
    author_obj = Pengajar.objects.get(user = request.user)
    courses = Course.objects.filter(author=author_obj)
    context = {'courses':courses}
    return render(request,'main/coursesbyme.html', context)

@login_required(login_url="/login")
def coursestaken(request):
    murid_obj = Murid.objects.get(user = request.user)
    courses = Course.objects.filter(murid=murid_obj)
    context = {'courses':courses}
    return render(request,'main/coursesbyme.html', context)

@login_required(login_url="/login")
def course_detail(request, id):
    course = Course.objects.get(id = id)
    sections= Section.objects.filter(course=course)
    forums= ForumPost.objects.filter(course=course)

    context = {'course':course,  'sections': sections, 'forums':forums}

    return render(request,'main/courseID.html', context)

def course_summary(request,id):
    course = Course.objects.get(id = id)
    summarys = SummaryPoints.objects.filter(course=course)
    context = {'course':course, 'summarys':summarys}
    return render(request, 'main/course_summary.html', context)

@login_required(login_url="/login")
def course_enroll(request,id):
    murid = request.user.murid
    course = Course.objects.get(id = id)
    murid.course_taken.add(course)
    # murid.save()

    return course_detail(request,id)

@login_required(login_url="/login")
def course_unenroll(request,id):
    murid = request.user.murid
    course = Course.objects.get(id = id)
    murid.course_taken.remove(course)

    return all_courses(request)
@login_required(login_url="/login")
def detail_view(request):
    # dictionary for initial data with
    # field names as keys
    context ={}
  
    # add the dictionary during initialization
          
    return render(request, "main/detail_view.html", context)

@login_required(login_url="/login") 
# update view for details
def update_view(request, id):
    # dictionary for initial data with
    # field names as keys
    context ={}
 
    # fetch the object related to passed id
    obj = get_object_or_404(User, id = id)
 
    # pass the object as instance in form
    form = ProfileForm(request.POST or None, instance = obj)
    if request.method == 'POST':
    # save the data from the form and
    # redirect to detail_view
        if form.is_valid():
            form.save()
            return redirect("/")
    
    # add form dictionary to context
    context["form"] = form
 
    return render(request, "main/update_view.html", context)

@login_required(login_url="/login")   
def course_add_section(request,id):
    if request.method == 'POST':
        form = SectionForm(request.POST)
        if form.is_valid():
            user_author = request.user
            course_obj = Course.objects.get(id=id)
            form.save()
            latest = Section.objects.last()
            latest.course = course_obj
            latest.save()
            return redirect('/course/'+id)
        else :
            return JsonResponse({'status':'error', 'msg': 'Section gagal ditambahkan'}) 

    context = {'form':SectionForm(), 'course':Course.objects.get(id=id)}
    return render(request, 'main/addcourse.html', context)

@login_required(login_url="/login")
def course_add_summary(request,id):
    if request.method == 'POST':
        form = SummaryPointsForm(request.POST)
        if form.is_valid():
            user_author = request.user
            course_obj = Course.objects.get(id=id)
            form.save()
            latest = SummaryPoints.objects.last()
            latest.course = course_obj
            latest.save()
            return redirect('/course-summary/%s'%id)
        else :
            return JsonResponse({'status':'error', 'msg': 'Section gagal ditambahkan'}) 

    context = {'form':SummaryPointsForm(),'course':Course.objects.get(id=id)}
    return render(request, 'main/addSummary.html', context)  

@login_required(login_url="/login")
def course_add_post(request,id,sectionid):
    if request.method == 'POST':
        form = PostCreateForm(request.POST)
        if form.is_valid():
            course_obj = Course.objects.get(id=id)
            section_obj = Section.objects.get(id=sectionid)
            form.save()
            latest = Post.objects.last()
            latest.course = course_obj
            latest.section = section_obj
            latest.save()
            return redirect('/course/%s'%id)
        else :
            return JsonResponse({'status':'error', 'msg': 'Section gagal ditambahkan'}) 

    context = {'form':PostCreateForm(),'course':Course.objects.get(id=id), 'section':Section.objects.get(id=sectionid)}
    return render(request, 'main/add_post.html', context)  

def participants_json(request,id):
    data = serializers.serialize('json', User.objects.all())
    return HttpResponse(data, content_type='application/json')
    # return HttpResponse(Murid.objects.all())

def contents_json(request,id):
    data = serializers.serialize('json', Course.objects.filter(id=id))
    return HttpResponse(data, content_type='application/json')

def summary_json(request,id):
    data = serializers.serialize('json', SummaryPoints.objects.filter(course=id))
    return HttpResponse(data, content_type='application/json')
def view_json(request,id):
    data = serializers.serialize('json', User.objects.filter(id=id))
    return HttpResponse(data, content_type='application/json')
    
