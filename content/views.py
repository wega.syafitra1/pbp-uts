from django.shortcuts import render, redirect, get_object_or_404
from django.http.response import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.views.decorators.http import require_POST
from .models import Content
from .forms import ContentForm
from main.models import Course
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

# Create your views here.
@login_required(login_url = '/login/')
def index(request, courseid):
    course = Course.objects.get(id = courseid)
    content = Content.objects.filter(course = course)
    response = {'contents': content}
    return render(request, 'content_index.html', response)

@login_required(login_url = '/login/')
def add_content(request, courseid):
    form = ContentForm(request.POST)
    if request.is_ajax():
        form = ContentForm(request.POST)
        if (request.user.is_pengajar):
            if (form.is_valid()):
                form.save()
                course = Course.objects.get(id=courseid)
                latest = Content.objects.last()
                latest.course = course
                latest.save()
                return JsonResponse({'message': 'success'})
            else:
                return JsonResponse({'message': 'failed'})
    response = {'form': form}
    return render(request, 'add_content.html', response)

@csrf_exempt
def delete_content(request, courseid, pk):
    content = Content.objects.get(id=pk)
    if (request.user.is_pengajar):
        if (request.method == "POST"):
            content.delete()
            return redirect('/content/main/course/%s/'%courseid)
    response = {}
    return render(request, 'delete_content.html', response)