from django.db import models
from main.models import *
# Create your models here.
class Content(models.Model):
    course = models.ForeignKey(Course, null=True, on_delete = models.CASCADE)
    title = models.CharField(max_length=50)
    file = models.URLField(null=True)

    def __str__(self):
        return self.title
