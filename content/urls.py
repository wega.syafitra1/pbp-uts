from django.urls import path, include
from django.conf.urls import url
from . import views

app_name = 'content'

urlpatterns = [
    path('^course/<id>/', include('main.urls')),
    path('main/course/<courseid>/', views.index, name='index'),
    path('main/course/<courseid>/add-content/', views.add_content, name='add_content'),
    path('', views.index, name='index'),
    url(r'^delete/(?P<courseid>\d+)/(?P<pk>\d+)/', views.delete_content, name='delete_content'),
]